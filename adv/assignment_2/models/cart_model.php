<?php

// load models
require APP . '/models/book_model.php';

// cart model

define('PST', .08);
define('GST', .05);

/**
* Add an item to the cart
* @param PDO $dbh Database connection
* @param Int $book_id the book to add
*/
function addToCart($dbh, $book_id)
{
	$book = getBook($dbh, $book_id);
	$cart = [
		'title' => $book['title'],
		'price' => $book['price'],
		'author' => $book['author']
	];
	$_SESSION['cart'] = $cart;

}