<?php
/**
 * Server page responsible for AJAX request
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);

define('ERRORKEY', 'error');

require APP . '/models/book_model.php';


$nonAjaxError[ERRORKEY] = "As you can see this assignment is NOT allowed me to receive any NON AJAX requests!";
$emptyError[ERRORKEY] = "Sorry, we cannot find the data with your given request!";
$invalidFormatError[ERRORKEY] = "Sorry: No book is found.";

// --------------------------------------------------------------------

/**
 * Make sure this is really an AJAX call
 *
 * @return  bool: indicates that there is an AJAX call or not
 */ 
function isXmlHttpRequest() {
    // Steve, we cannot find HTTP_X_REQUESTED_WITH but X-Requested-With
    $headers = getallheaders();
    $header = array_key_exists('X-Requested-With', $headers) ? $headers['X-Requested-With'] : null;
    
    // This is recommended or NOT?
    //$header = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : null;
    return ($header === 'XMLHttpRequest');
}

// --------------------------------------------------------------------

/**
 * The main entry of processing requests
 *
 * @return  array: an array will be returned, indicating error or results
 */
function proceedRequests() {
  global $dbh;
  global $nonAjaxError;
  global $emptyError;
  global $invalidFormatError;

  // As requirement: NON Ajax is treated as an invalid request
  // Why we have to make our lives so complicated like this?
  if(!isXmlHttpRequest()) {
    return $nonAjaxError;
  }

  // Get keyword and prevent XSS attach
  $keyword = isset($_GET['keyword']) ? htmlspecialchars($_GET['keyword'], ENT_QUOTES, 'UTF-8', false) : '';
  
  // Is limit parameter set? Go with retrieveSeveralBooks
  if ($keyword == '') {  
      return $invalidFormatError;
  } else {
    // Get the book info, limit is hard coded to 5 as requirement
    $result = searchBooks($dbh, $keyword, 5);
    if (count($result)>0) {
      // Return the books info
      return $result;
    } else {
      // Nothing found
      return $emptyError;
    }
    
  }
  
  // Reach here? that means nothing we can help
  return $invalidFormatError;
	
}
// --------------------------------------------------------------------

// Enable JSON type when returned
header('Content-type: application/json');

// Dump it out
echo json_encode(proceedRequests());


?>