<?php

// load config
require __DIR__ . '/../config/config.php';

// load models
require APP . '/models/cart_model.php';

// Make sure we have a POST request
// Commented out so students can view
// this file in the code repository
/*
if($_SERVER['REQUEST_METHOD'] != 'POST') {
	die('Sorry, you are in the wrong place.');
}
*/

// Make sure there's a cart or create one
if(!isset($_SESSION['cart'])) {
	$_SESSION['cart'] = array();
}

// If we have the 
if(!empty($_POST['book_id'])) {
	addToCart($dbh, $_POST['book_id']);
	header('Location: ' . $_SERVER['HTTP_REFERER']);
	die;
}



