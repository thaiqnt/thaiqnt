<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

// Create a new PDO instanace
try {
  $dbh = new PDO('sqlite:../database.sqlite');
  // Set errormode to exceptions
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
// Catch any errors
catch (PDOException $e) {
  echo $e->getMessage();
  exit();
}


