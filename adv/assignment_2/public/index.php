<?php

// start output buffering
ob_start();

// start session
session_start();

// define APP path
define('APP', __DIR__ . '/..');

// load config
require APP . '/config.php';

if(empty($_GET['p'])) {
	$_GET['p'] = 'home';
}

// define allowed routes
$allowed = array('about', 'books', 'cart', 'detail', 'home', 'contact', 'server');

// load route controller or load error controller
if(in_array($_GET['p'], $allowed)) {
	require APP . '/controllers/' . $_GET['p'] . '.php';
} else {
	die('Error 404 Not Found');
}
// flush output buffer

