<?php

// include templates
include __DIR__ . '/inc/header.inc.php';
include __DIR__ . '/inc/banner.inc.php';
include __DIR__ . '/inc/search.inc.php';

?>

<h1><?php echo $title; ?></h1>
<script>
  // Yes, this is a books page
  isBooksPage = true;
</script>
<?php include __DIR__ . '/inc/cart.inc.php'; ?>

	<?php include __DIR__ . '/inc/categories.inc.php'; ?>

	<div id="searchresult" class="shelf">

		<?php foreach($books as $row) : ?>

		<div class="book">

			<div class="img">
				<img src="images/covers/<?php echo $row['image']; ?>" alt="<?php echo $row['title']; ?>" />
			</div>
			<div class="details">
				<p><strong><?php echo $row['title']; ?></strong><br />
					by <a href="?p=books&author_id=<?php echo $row['author_id'] ?>"><?php echo $row['author']; ?></a><br />
					<span>Horror</span>, <?php echo $row['num_pages']; ?> pages, <?php echo $row['year_published']; ?>, $<?php echo $row['price']; ?></p>
					<?php echo substr($row['description'], 0, 100); ?>
					<p><a class="more" href="?p=detail&book_id=<?php echo $row['book_id']; ?>">More info</a>
			</div>

		</div><!-- /.book -->

	<?php endforeach; ?>

	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php include __DIR__ . '/inc/footer.inc.php'; ?>


