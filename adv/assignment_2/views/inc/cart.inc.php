<?php if(!empty($_SESSION['cart'])) : ?>

<div class="cart">
<p><strong>Your cart:</strong><br />
<small>You have 1 item in your cart:</small><br />
<small><?=$_SESSION['cart']['title']?>, by <?=$_SESSION['cart']['author']?>.   $<?=$_SESSION['cart']['price']?></small><br />
<small><a href="/?p=checkout">Checkout now!</a></small></p>
</div>

<?php endif; ?>