<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $title ?></title>
	<link rel="stylesheet" type="text/css" href="css/style.css?v=<?=rand(1000,9000)?>" />
    <style>
      .bookitem {
        border: 0;
        margin: 0;
        padding: 0;
      }
      .bookitem:hover {
          background-color: #ccc;
      }      
    </style>
</head>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
      
      // Assume that every page is NOT books, except itself!
      var isBooksPage = false;
    
      // Search book by providing a keyword
      function searchBooksByButton(data) {
        // Change the search title with correct keyword
        $('h1').html('You searched for: '+data);
        
        // Clean up the search input when users hit search button
        $('#keyword').val('');
        $('#listid').empty();
        
        // Our destination is search result in book view
        var showData = $('#searchresult');

        // We provide p as page and keyword as title or author search input
        $.getJSON("index.php", {p: 'server', keyword: data}, function (list,status,xhr) {

          // Clear all previous data if it is available
          showData.empty();

          var content = '';

          // Everything is OK?
          if (status == 'success') {
            // The server indicates that there is an error?
            if (list.hasOwnProperty('error')) {
              // There is an error posted back        

              // Show that error
              content = '<h3>' + list.error + '</h3>';
              showData.append(content);
            } else {
              // Go ahead with really correct result
                            
              // Populate book labels
              for (var i=0; i<list.length; i++) {
                content  = '<div class="book">';
                content += '  <div class="img">';
                content += '    <img src="images/covers/' + list[i].image + '" alt="' + list[i].title + '" />';
                content += '  </div>';
                content += '  <div class="details">';
                content += '    <p><strong>' + list[i].title + '</strong><br />';
                content += '      by <a href="?p=books&author_id=' + list[i].author_id + '">' + list[i].author + '</a><br />';
                content += '      <span>Horror</span>, ' + list[i].num_pages + ' pages, ' + list[i].year_published + ', $' + list[i].price + '</p>';
                content += '      ' + list[i].description.substr(0,100);
                content += '      <p><a class="more" href="?p=detail&book_id=' + list[i].book_id + '">More info</a>';
                content += '  </div>';
                content += '</div><!-- /.book -->';
                
                showData.append(content);
              }
              
              
            }
          } else {
            // Show that error
            content = '<h3>Unable to connect the server, please check your network/configuration.</h3>';
            showData.append(content);
            return;
          }


        }).fail(function(){
            // Clear the ouput
            showData.empty();
            // Show that error
            content = '<h3>Unable to connect the server, please check your network/application.</h3>';
            showData.append(content);
            return;
        });


      }

      // Search books by typing a keyword
      function searBooksByTyping(data) {
        var showData = $('#listid');

        // We provide p as page and keyword as title or author search input
        $.getJSON("index.php", {p: 'server', keyword: data}, function (list,status,xhr) {

          // Clear all previous data if it is available
          showData.empty();

          // Define a content
          var content = '';

          // Everything is OK?
          if (status == 'success') {
            // The server indicates that there is an error?
            if (list.hasOwnProperty('error')) {
              // There is an error posted back        

              // Show that error
              content = '<h3>' + list.error + '</h3>';
              // Users really annoy to see this kind of error during their search
              // We simply give it to console log instead of letting them notice it
              //showData.append(content);
              console.log(content);
            } else {
              // Go ahead with really correct result

              // Populate book labels
              for (var i=0; i<list.length; i++) {
                content = '<p class="bookitem" dataid="' + list[i].book_id + '">' + list[i].title + '</p>';
                showData.append(content);
              }

              // Add onclick handlerd to each book link
              $('#listid p').each(function() {

                $(this).click(function() {
                  var bookid = $(this).attr('dataid');
                  //console.log(bookid);
                  // Go to the detail of this book
                  window.location = '?p=detail&book_id='+bookid;
                });
              });
            }
          } else {
            // Show that error
            content = '<h3>Unable to connect the server, please check your network/configuration.</h3>';
            // Users really annoy to see this kind of error during their search
            // We simply give it to console log instead of letting them notice it
            //showData.append(content);
            console.log(content);
            return;
          }


        }).fail(function(){
            // Clear the ouput
            showData.empty();
            // Show that error
            content = '<h3>Unable to connect the server, please check your network/application.</h3>';
            // Users really annoy to see this kind of error during their search
            // We simply give it to console log instead of letting them notice it
            //showData.append(content);
            console.log(content);
            return;
        });


      }

      // Here is the main entrance to our code
      $(document).ready(function() {
        // bind function called when key is released
        $('#keyword').keyup(function(event) {   
          searBooksByTyping($('#keyword').val());
        });
        
        // Prevent submission in search form
        $('#searchform').submit(function(e){
            // In the books page, we use our function to update the search list. 
            // In other pages like detail, we use the submission to get the result
            if (isBooksPage) {
                e.preventDefault();
            }
        });
        
        // Implement search function
        $('#searchbutton').click(function(event) {   
            // In the books page, we use our function to update the search list. 
            // In other pages like detail, we use the submission to get the result
            if (isBooksPage) {
                searchBooksByButton($('#keyword').val());
            }
        });
      });

    </script>
<body>

<div class="container">

	<div id="header">

	<nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="?p=home">home</a></li><li>
			<a href="?p=books">books</a></li><li>
			<a href="?p=about">about</a></li><li>
			<a href="?p=contact">contact</a></li>
		</ul>

	</nav>

	</div><!-- /#header -->
