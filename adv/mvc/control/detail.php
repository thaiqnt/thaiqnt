<?php 

// if no book ID, send back to books page
if(empty($_GET['book_id'])) {
	header('Location: books.php');
	die;
}

// load models
require MODEL . 'book_model.php';
require MODEL . 'genre_model.php';

// get all the data we need to build the page
$book = getBook($pdo, $_GET['book_id']);
$genres = getGenres($pdo);
$publisher_books = getBooksByPublisher($pdo, $book['publisher_id']);

// use this for first part of flag image name
$flag = strtolower($book['country']);

// use this for first part of author image name
$author = strtolower(str_replace(' ', '_', $book['author']));

require VIEW . 'detail.php';

?>