<?php 

require MODEL . 'book_model.php';
require MODEL . 'genre_model.php';

// get whatever kind of book list the user wants
if(!empty($_GET['author_id'])) {
	$books = getBooksByAuthor($pdo, $_GET['author_id']);
	$title = 'Books by Author: ' . $books[0]['author'];
} elseif(!empty($_GET['genre_id'])) {
	$books = getBooksByGenre($pdo, $_GET['genre_id']);
	$title = 'Books by Genre: ' . $books[0]['genre'];
} elseif(!empty($_GET['s'])) {
	$books = search($dbh, $_GET['s']);
	$title = 'You searched for: ' . htmlspecialchars($_GET['s']);
} else {
	$books = getRandomBooks($pdo, 5);
	$title = 'Books you might like...';
}

if(empty($books)) {
	$title = 'No books matched your query';
}

// genre list for sidebar
$genres = getGenres($pdo);

require VIEW . 'books.php';
?>