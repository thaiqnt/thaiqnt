<?php 


// include templates
include VIEW . '/inc/header.inc.php';
include VIEW . '/inc/banner.inc.php';
include VIEW . '/inc/search.inc.php';

?>

<h1><?php echo $title; ?></h1>

	<?php include VIEW . '/inc/categories.inc.php'; ?>

	<div class="shelf">

		<?php foreach($books as $row) : ?>

		<div class="book">

			<div class="img">
				<img src="images/covers/<?php echo $row['image']; ?>" alt="<?php echo $row['title']; ?>" />
			</div>
			<div class="details">
				<p><strong><?php echo $row['title']; ?></strong><br />
					by <a href="?p=books.php&author_id=<?php echo $row['author_id'] ?>"><?php echo $row['author']; ?></a><br />
					<span>Horror</span>, <?php echo $row['num_pages']; ?> pages, <?php echo $row['year_published']; ?>, $<?php echo $row['price']; ?></p>
					<?php echo substr($row['description'], 0, 100); ?>
					<p><a class="more" href="?p=detail.php&book_id=<?php echo $row['book_id']; ?>">More info</a>
			</div>

		</div><!-- /.book -->

	<?php endforeach; ?>

	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php include VIEW . '/inc/footer.inc.php'; ?>

