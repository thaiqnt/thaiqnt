<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php echo $title ?></title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>

<div class="container">

	<div id="header">

	<nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="?p=home.php">home</a></li><li>
			<a href="?p=books.php">books</a></li><li>
			<a href="?p=about.php">about</a></li><li>
			<a href="?p=contact.php">contact</a></li>
		</ul>

	</nav>

	</div><!-- /#header -->




	<?php 
// Show source of script, but only if this file is
// loaded directly... not if it's included in another file
if(basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
	show_source(__FILE__);
} 
?>