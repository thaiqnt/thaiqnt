<?php
/**
 * Server page responsible for AJAX request
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);


// All database declarations should be here
$db['default']['dbdriver'] = "mysql";
$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "mysql";
$db['default']['database'] = "booksite";
$db['default']['charset'] = "utf8";

define('ERRORKEY', 'error');

$nonAjaxError[ERRORKEY] = "As you can see this assignment is NOT allowed me to receive any NON AJAX requests!";
$emptyError[ERRORKEY] = "Sorry, we cannot find the data with your given request!";
$invalidFormatError[ERRORKEY] = "Invalid format was found in the submitted request.";

define('APP', __DIR__ . DIRECTORY_SEPARATOR);

$dsn = "{$db['default']['dbdriver']}:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['charset']}";

$options = [
  PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
];


// Setting up the database connection
try {
  $pdo = new PDO($dsn, $db['default']['username'], $db['default']['password'], $options);
} catch (Exception $e) {
  error_log($e->getMessage());
  
  // Must comply JSON format when returning an error
  $pdoError[ERRORKEY] = $e->getMessage();
  die(json_encode($pdoError));
}


/**
 * getArrayForForm
 *
 * This function returns an array valid for a form
 *
 * @param   arr : an array to be valid for form
 * @return  array: an array after being valid
 */
function getArrayForForm($arr) {
    $result = array();

    foreach ($arr as $key => $value) {
        $result[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false);
    }

    return $result;
}
// --------------------------------------------------------------------



/**
 * Get all of book information by randomization
 *
 * @param  limit: maximum books will be returned
 * @return  array: an array of book information
 */
 function retrieveSeveralBooks($limit) {
    global $pdo;

    // Prepare the query
    $stmt = $pdo->prepare("SELECT
	book.title,
	book.book_id,
	book.description,
	book.num_pages,
	book.year_published,
	book.price,
	book.image,
	author.name as author,
	author.author_id,
	genre.name as genre,
	format.name as format
	FROM
	book
	JOIN author USING(author_id)
	JOIN publisher USING(publisher_id)
	JOIN genre USING(genre_id)
	JOIN format USING(format_id)
	ORDER BY book.book_id
	LIMIT :limit");
                          
                            
    // Litmits the result
	$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);

	$stmt->execute();

	// get several books
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
	
}
// --------------------------------------------------------------------

/**
 * Get all of book information if possible by its id
 *
 * @param  bookId: id of a book
 * @return  array: an array of book information
 */
function retrieveBook($bookId) {
    global $pdo;

    // Prepare the query
    $stmt = $pdo->prepare("SELECT
	book.title,
	book.book_id,
	book.description,
	book.num_pages,
	book.year_published,
	book.price,
	book.image,
	author.name as author,
	author.author_id,
	author.country,
	book.in_print,
	genre.name as genre,
	format.name as format,
	publisher.name as publisher,
	publisher.city as city,
	publisher.publisher_id
	FROM
	book
	JOIN author USING(author_id)
	JOIN publisher USING(publisher_id)
	JOIN genre USING(genre_id)
	JOIN format USING(format_id)
	WHERE
	book_id = ?");
    // Do it with id
    $stmt->execute([$bookId]);
    // Get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Free all resource allocated
    $stmt = null;


    // To prevent XSS attacks
    if (count($result) > 0) {
      // There is only one book found, pick one row only
      $result = getArrayForForm($result[0]);
    }

    // Get back the result
    return $result;
}
// --------------------------------------------------------------------

/**
 * Validate Numeric input
 *
 * @param   str: a string to be checked
 * @return  bool: a boolean value should be returned indicating it is OK or not
 */ 
function isnumeric($str) {
    return ( ! preg_match("/^([0-9])+$/", $str)) ? false : true;
}
// --------------------------------------------------------------------


/**
 * Make sure this is really an AJAX call
 *
 * @return  bool: indicates that there is an AJAX call or not
 */ 
function isXmlHttpRequest() {
    // Steve, we cannot find HTTP_X_REQUESTED_WITH but X-Requested-With
    $headers = getallheaders();
    $header = array_key_exists('X-Requested-With', $headers) ? $headers['X-Requested-With'] : null;
    
    // This is recommended or NOT?
    //$header = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : null;
    return ($header === 'XMLHttpRequest');
}

// --------------------------------------------------------------------

/**
 * The main entry of processing requests
 *
 * @return  array: an array will be returned, indicating error or results
 */
function proceedRequests() {
  global $nonAjaxError;
  global $emptyError;
  global $invalidFormatError;

  // As requirement: NON Ajax is treated as an invalid request
  // Why we have to make our lives so complicated like this?
  if(!isXmlHttpRequest()) {
    return $nonAjaxError;
  }

  // Get ids
  $limit = isset($_GET['limit']) ? $_GET['limit'] : '';
  $bookid = isset($_GET['bookid']) ? $_GET['bookid'] : '';
  
  // Is limit parameter set? Go with retrieveSeveralBooks
  if ($limit != '') {  
    // Is valid id?
    if (isnumeric($limit)) {
      // Get the book info
      $result = retrieveSeveralBooks($limit);
      if (count($result)>0) {
        // Return the books info
        return $result;
      } else {
        // Nothing found
        return $emptyError;
      }

    } else {
      return $invalidFormatError;
    }
  }
  
  // Is bookid parameter set? Go with retrieveBook
  if ($bookid != '') {
    // Is valid id?
    if (isnumeric($bookid)) {
      // Get the book info
      $result = retrieveBook($bookid);
      if (count($result)>0) {
        // Return the book info
        return $result;
      } else {
        // Nothing found
        return $emptyError;
      }
    } else {
      // Unable to understand the request
      return $invalidFormatError;
    }
  }
  
  // Reach here? that means nothing we can help
  return $invalidFormatError;
	
}
// --------------------------------------------------------------------

// Enable JSON type when returned
header('Content-type: application/json');

// Dump it out
echo json_encode(proceedRequests());


?>