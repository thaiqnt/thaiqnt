<?php

require 'ArrayToXml.php';


$user = [
'firstname' => 'Thai',
'lastname' => 'Tran',
'email' => 'thaiqnt@gmail.com',
'hobbies' => [
  'reading', 'listening', 'writing'
  ]
];


$xml = new Spatie\ArrayToXml\ArrayToXml($user, 'user');

$x = $xml->toXml();

header('Content-type: application/xml');

echo $x;
?>