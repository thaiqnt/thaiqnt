<?php

$user = [
'firstname' => 'Thai',
'lastname' => 'Tran',
'email' => 'thaiqnt@gmail.com',
'hobbies' => [
  'reading', 'listening', 'writing'
  ]
];

header('Content-type: application/json');

echo json_encode($user);

?>