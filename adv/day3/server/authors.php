<?php
// authors.php


// --------------------------------------------------------------------


/**
 * getArrayForForm
 *
 * This function returns an array valid for a form
 *
 * @param   arr : an array to be valid for form
 * @return  array: an array after being valid
 */
function getArrayForForm($arr) {
    $result = array();

    foreach ($arr as $key => $value) {
        $result[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false);
    }

    return $result;
}
// --------------------------------------------------------------------

/**
 * Validate Numeric input
 *
 * @param   str: a string to be checked
 * @return  bool: a boolean value should be returned indicating it is OK or not
 */ 
function isnumeric($str) {
    return ( ! preg_match("/^([0-9])+$/", $str)) ? false : true;
}
// --------------------------------------------------------------------

/**
 * Get all of author information if possible by its id
 *
 * @param  authorId: id of a author
 * @return  array: an array of author information
 */
function retrieveAuthor($authorId) {
    global $pdo;

    // Prepare the query
    $stmt = $pdo->prepare("SELECT
    author_id as authorid,
	name,
	country,
	image
	FROM
	author
    WHERE
    author_id = ?");
    // Do it with id
    $stmt->execute([$authorId]);
    // Get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Free all resource allocated
    $stmt = null;


    // To prevent XSS attacks
    if (count($result) > 0) {
      // There is only one book found, pick one row only
      $result = getArrayForForm($result[0]);
    }

    // Get back the result
    return $result;
}
// --------------------------------------------------------------------
/**
 * Get all of author information
 *
 * @return  array: an array of author information
 */
function listAuthors() {
    global $pdo;

    // Prepare the query
    $stmt = $pdo->prepare("SELECT
    author_id as authorid,
	name,
	country,
	image
	FROM
	author");
    // Do it with id
    $stmt->execute();
    // Get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Free all resource allocated
    $stmt = null;


    $refinedResult = array();
    
    // To prevent XSS attacks
    foreach ($result as $key => $value) {
    
      $refinedResult[$key] = getArrayForForm($value);
    }

    // Get back the result
    return $refinedResult;
}
// --------------------------------------------------------------------
/**
 * Show all of author information
 *
 * @return  string: an html of the list will be returned
 */
function printAuthors($list) {
  $result = '';
  
  if (count($list)>0) {
  
    $result = "<ul>";
    foreach ($list as $author) {
      
      $result .= "<li data-id=\"{$author['authorid']}\">{$author['name']}</li>";  
    }
    $result .= "</ul>";
  }
  
  return $result;
}

// --------------------------------------------------------------------
/**
 * Show all of author information
 *
 * @param   id: an id of author
 * @return  string: an html of the list will be returned
 */
function printAnAuthor($author) {
  $result = '';
  
  $result = "<ul>";

  $result .= "<li data-id=\"{$author['authorid']}\">{$author['name']}</li>".
            "<li>{$author['country']}</li>".
            "<li>{$author['image']}</li>";  
  $result .= "</ul>";
  
  return $result;
}


// Setting up the database connection
try {
    $pdo = new PDO('sqlite:database1.sqlite');
    // Set errormode to exceptions
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (Exception $e) {
    error_log($e->getMessage());
    die($e->getMessage());
}

// Get the authorid
$authorid = isset($_GET['authorid']) ? $_GET['authorid'] : '';

$list = '';

// if authorid is set, select that author or select all authors
if ($authorid == '') {
  $list = printAuthors( listAuthors() );
} else {
    if (isnumeric($authorid)) {
        $list = printAnAuthor( retrieveAuthor($authorid) );
    }
}

// if single author  , print detail
// or print list of authors 



echo $list;
?>