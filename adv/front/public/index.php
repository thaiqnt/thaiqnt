<?php
/**
 * Index module
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

require ('../config.php');

session_start();


ob_start();


checkValidLinks(isset($_GET['p']) ? $_GET['p'] : '');


function checkValidLinks($param) {
  global $pdo;
  global $allowedLinks;
  
  // These conditions are the same as !$isLoggedin and !in_array($filename, $allowedLinks)
  if (in_array($param, $allowedLinks)) {
    require(PAGE.$param);
  } else {
    // These links will be redirected to login page if there is no user logged in except directLinks
    require(PAGE."error.php");
  }
}



?>