<?php
/**
 * Error Page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------


$content = <<<EOT
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Bookstore</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>

<div class="container">

	<div id="header">

	<nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="index.html">home</a></li><li>
			<a href="books.html">books</a></li><li>
			<a href="about.html">about</a></li><li>
			<a href="contact.html">contact</a></li>
		</ul>

	</nav>

	</div><!-- /#header -->

	<div class="header_img">
		<img src="images/header.jpg" />
	</div>

			<div class="search">

			<form>
				<input type="text" name="s" maxlength="255" />&nbsp;
				<input type="submit" value="search" />
			</form>
		</div>

		<hr class="clear" />

		<h1>Error: You are here cause we cannot serve the page you are looking for...</h1>

	

</div<!-- /.container -->

</body>
</html>
EOT;

echo $content;
?>