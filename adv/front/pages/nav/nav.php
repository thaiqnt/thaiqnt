<?php
/**
 * Nav Page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------


$navpage = <<<EOT
	<nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="?p=index.php">home</a></li><li>
			<a href="?p=books.php">books</a></li><li>
			<a href="?p=about.php">about</a></li><li>
			<a href="?p=contact.php">contact</a></li>
		</ul>

	</nav>
EOT;
?>