<?php

// cart model

define('PST', .08);
define('GST', .05);

$message = false;

if(filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {
	if($book_id = filter_input(INPUT_POST, 'book_id', FILTER_VALIDATE_INT)) {

		if(add_item_to_cart($dbh, $book_id)) {
			$message = 'Item added to cart!';
		}
	}
}

// SIMPLE ADD TO CART - ALLOWS ONE ITEM
function add_to_cart($dbh, $book_id)
{
	if(!$book = getBookById($dbh, $book_id)) {
		$message = 'Sorry, we could not add item to cart!';
		return;
	}

	if(!isset($_SESSION['cart'])) {
		$_SESSION['cart'] = array();
	}

	$_SESSION['cart'] = $book;

}

function getPST()
{
	if(!empty($_SESSION['cart']) {
		return round($_SESSION['cart']['price'] * PST, 2);
	} 
	return 0;
}

function getGST()
{
	if(!empty($_SESSION['cart']) {
		return round($_SESSION['cart']['price'] * GST, 2);
	} 
	return 0;
}

function getTotal()
{
	if(!empty($_SESSION['cart']) {
	$pst = getPST();
	$gst = getGST();
	$sub = $_SESSION['cart']['price'];
	return round($pst + $gst + $sub, 2);
	}
	return 0;
}

