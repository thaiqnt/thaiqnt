<?php
/**
 * Configuration module
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);

define('BOOK_CONFIG_PATH', __DIR__ . '/');


// All database declarations should be here
$db['default']['dbdriver'] = "mysql";
$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "mysql";
$db['default']['database'] = "booksite";
$db['default']['charset'] = "utf8";


define('APP', __DIR__ . DIRECTORY_SEPARATOR);
define('PAGE', APP . 'pages' . DIRECTORY_SEPARATOR);

// These links are not affected by rules of checking login
$allowedLinks = array(
  'index.php', 'login.php', 'logout.php', 'register.php', 'about.php', 'books.php', 'contact.php', 'detail.php');


$dsn = "{$db['default']['dbdriver']}:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['charset']}";

$options = [
  PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
];


// Setting up the database connection
try {
  $pdo = new PDO($dsn, $db['default']['username'], $db['default']['password'], $options);
} catch (Exception $e) {
  error_log($e->getMessage());
  die($e->getMessage());
}


?>