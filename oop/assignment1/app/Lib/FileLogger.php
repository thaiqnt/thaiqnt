<?php
/**
 * Created by Thai
 * Date: 18-10-05
 * Time: 1:08 PM
 */

namespace App\Lib;

class FileLogger implements ILogger
{
    // Internal filename
    private $filename;
    
    // Add to the internal filename
    public function __construct($filename) 
    {
      $this->filename = $filename;
    }
    
    // Override function to write log statement
	public function write($event)
    {
      $content = file_get_contents($this->filename);
      file_put_contents($this->filename, $content."\r\n".$event);
    }

}