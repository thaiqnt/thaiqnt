<?php
/**
 * Created by Thai
 * Date: 18-10-05
 * Time: 1:08 PM
 */

namespace App\Lib;

class DatabaseLogger implements ILogger
{
    // Internal connection
    private $conn;
    
    // Construct internal connection
    public function __construct($conn) 
    {
      $this->conn = $conn;
    }
    
    // Override function to write log statement
	public function write($event)
    {
      // Create a new event for a log
      $stmt = $this->conn->prepare("INSERT INTO `events`
        (`event`)
          VALUES
        (:event)"
        );
      // Bind it to string field
      $stmt->bindParam(':event', $event, \PDO::PARAM_STR);

      // Do it 
      $stmt->execute();
      // Free all resource allocated
      $stmt = null;
    }

}