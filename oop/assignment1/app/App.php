<?php
/**
 * App Class
 * Front Controller Class for Assignment 1
 */

namespace App;

class App
{

	/**
	 * IOC container
	 */
	private $c;

	/**
	 * @param Pimple $container
	 */
	public function __construct(\Pimple\Container $container)
	{
		$this->c = $container;
	}

	/**
	 * Run the app
	 */
	public function run()
	{
		$c = $this->c;
		$this->log($c['logger']);
	}

	/**
	 * Log requests
	 */
	public function log(\App\Lib\ILogger $logger)
	{
		// do something to concat an $event string
		// Hint... most info can be found in $_SERVER
        
        $event = date('Y-m-d H:i:s') . "|".$_SERVER['REMOTE_ADDR']."|".$_SERVER['REQUEST_METHOD']."|".http_response_code()."|".$_SERVER['HTTP_USER_AGENT'];
		


		/* DO NOT EDIT BELOW THIS LINE
		--------------------------------------------------- */

		echo "<p>Logged using " . get_class($logger) . ":</p>";
		var_dump($event);
		$logger->write($event);
		
	}


}
