<?php

namespace App\Lib;

class Input implements IInput, IInputCookie, IInputGet, IInputPost, IInputServer 
{
  // Use a trait as requirement
  use Traits\Helper;
  
  /**
  * If $key is null, return full COOKIE array.
  * If $key is not null, return the COOKIE value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function cookie($key = null, $clean = false) 
  {
    // For a specific key
    if (isset($key)) {
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_COOKIE[$key]);
      } else {
        // Get raw values
        return $_COOKIE[$key];
      }
    } else {
      // For the POST variable only
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_COOKIE);
      } else {
        // Get raw values
        return $_COOKIE;
      }
    }
    
  }
  
  /**
  * If $key is null, return full GET array.
  * If $key is not null, return the GET value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function get($key = null, $clean = false)
  {
    // For a specific key
    if (isset($key)) {
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_GET[$key]);
      } else {
        // Get raw values
        return $_GET[$key];
      }
    } else {
      // For the POST variable only
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_GET);
      } else {
        // Get raw values
        return $_GET;
      }
    }
    
  }
  
  /**
  * If $key is null, return full POST array.
  * If $key is not null, return the POST value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function post($key = null, $clean = false)
  {
    // For a specific key
    if (isset($key)) {
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_POST[$key]);
      } else {
        // Get raw values
        return $_POST[$key];
      }
    } else {
      // For the POST variable only
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_POST);
      } else {
        // Get raw values
        return $_POST;
      }
    }
    
  }
  
  /**
  * If $key is null, return full SERVER array.
  * If $key is not null, return the SERVER value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function server($key = null, $clean = false)
  {
    // For a specific key
    if (isset($key)) {
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_SERVER[$key]);
      } else {
        // Get raw values
        return $_SERVER[$key];
      }
    } else {
      // For the POST variable only
      if ($clean) {
        // Get clean values
        return $this->getArrayForForm($_SERVER);
      } else {
        // Get raw values
        return $_SERVER;
      }
    }
    
  }
}