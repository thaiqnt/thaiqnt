<?php

namespace App\Lib\Traits;

trait Helper 
{
    /**
     * getArrayForForm
     *
     * This function returns an array valid for a form
     *
     * @param   arr : an array or a string to be valid for form
     * @return  array: an array after being valid
     */
    public function getArrayForForm($arr) {
        $result = array();
        
        // For array input
        if (is_array($arr)) {
          foreach ($arr as $key => $value) {
              if (is_array($value)) {
                // Call this function for get array input
                $result[$key] = $this->getArrayForForm($value);
              } else {
                // Get safe string
                $result[$key] = htmlspecialchars(strip_tags($value), ENT_QUOTES, 'UTF-8', false);            
              }
          }
        } else {
          // Get safe string
          $result = htmlspecialchars(strip_tags($arr), ENT_QUOTES, 'UTF-8', false);   
        }

        // Give the result
        return $result;
    }

  
}