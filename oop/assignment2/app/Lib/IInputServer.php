<?php

namespace App\Lib;

interface IInputServer
{

  /**
  * If $key is null, return full POST array.
  * If $key is not null, return the POST value for $key or NULL
  * if $clean is true, return XSS safe version of array or value
  * @param String $key
  * @return Mixed
  */
  public function server($key = null, $clean = false);

}