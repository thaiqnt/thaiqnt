<?php

// This is our home page
Route::get('/', 'PostsController@home');

// This is our welcome page
Route::get('/welcome', function () {
    return view('welcome');
});


// Added by auth command
Auth::routes();

// Normal post functions
Route::get('/home', 'HomeController@index')->name('home');

// Fix the bug of logged-out
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

// List all posts
Route::get('/posts', 'PostsController@index');

// The function below is available if an authenticated user logged in
Route::get('/posts/create', 'PostsController@create')->middleware('auth');

Route::get('/posts/archives/{year}/{month}', 'PostsController@archive');

// The function below is available if an authenticated user logged in
Route::post('/posts', 'PostsController@store')->middleware('auth');

// Edit a specific post
Route::get('/posts/{post}/edit', 'PostsController@edit');

// Update a specific post
Route::post('/posts/{post}/edit', 'PostsController@update');

// View a specific post
Route::get('/posts/{post}', 'PostsController@show');

// The function below is available if an authenticated user logged in
Route::post('/posts/{post}/comments', 'CommentsController@store')->middleware('auth');


// Secure all /admin/* urls as requirement

// Admin home page
Route::get('/admin', 'AdminController@index')->middleware('admin');

// Publish a draft post
Route::get('/admin/{post}/publish', 'AdminController@publish')->middleware('admin');

// Make a post as draft
Route::get('/admin/{post}/unpublish', 'AdminController@unpublish')->middleware('admin');

// Delete a post
Route::get('/admin/{post}/delete', 'AdminController@delete')->middleware('admin');

// Undelete a post
Route::get('/admin/{post}/undelete', 'AdminController@undelete')->middleware('admin');

// Edit a specific post
Route::get('/admin/{post}/edit', 'AdminController@edit')->middleware('admin');

// Update a specific post
Route::post('/admin/{post}/edit', 'AdminController@update')->middleware('admin');


