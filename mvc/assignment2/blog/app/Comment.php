<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Comment model
class Comment extends Model
{
    // Here is the whitelist
    protected $fillable = ['body', 'post_id', 'user_id'];
    
    // Get a post belonged to this comment
    public function post() {
      return $this->belongsTo(Post::class);
    }
}
