<?php

namespace App\Http\Middleware;

use Closure;


/**
 * Implementation of admin middleware.
 *
 */
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check the admin role
        if ( \Auth::check() && \Auth::user()->isAdmin() )
        {
            return $next($request);
        }

        // Deny the invalid request
        return redirect('/');
    }
}
