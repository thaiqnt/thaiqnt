<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

/**
 * Implementation of admin controller which handles all admin tasks.
 *
 */
class AdminController extends Controller
{
    // images and thumbnails array
    private $images;
    private $thumbs;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // List all images from folder images
        $list = glob("./img/image*.jpg");
        $this->images = array();
        foreach ($list as $l) {
          $this->images[] = substr($l, 6);
        }
        // List all thumbnails from folder images
        $list = glob("./img/thumb*.jpg");
        $this->thumbs = array();
        foreach ($list as $l) {
          $this->thumbs[] = substr($l, 6);
        }
        
    }
    
    
    /**
     * Show the application admin panel.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all user posts
        $posts = Post::latest()->paginate(5);
        
        // Set a returned url to come back as a flash message
        session()->put('backurl', url()->full());
        // Display them
        return view('admin.index', compact('posts'));
    }
    
    /**
     * Publish a draft post
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function publish(Post $post)
    {
      // Change the status as requested
      $post->status = 'published';
      $post->updated_at = new \DateTime;
      $post->save();
        
      // Given a page index?
      $page = request('page');
      if (isset($page)) {
        // Show a page in the list
        return redirect("/admin?page=$page");
      }
      
      // Show the list
      return $this->index();
    }
    
    /**
     * Make a post as draft
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function unpublish(Post $post)
    {
      // Change the status as requested
      $post->status = 'draft';
      $post->updated_at = new \DateTime;
      $post->save();
       
      // Given a page index?
      $page = request('page');
      if (isset($page)) {
        // Show a page in the list
        return redirect("/admin?page=$page");
      }
             
      // Show the list
      return $this->index();
    }
    
    /**
     * Make a post as draft
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function delete(Post $post)
    {
      // Change the is_deleted as requested
      $post->is_deleted = true;
      $post->updated_at = new \DateTime;
      $post->save();
      
      // Given a page index?
      $page = request('page');
      if (isset($page)) {
        // Show a page in the list
        return redirect("/admin?page=$page");
      }
        
      // Show the list
      return $this->index();
    }
    
    /**
     * Make a post as draft
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function undelete(Post $post)
    {
      // Change the is_deleted as requested
      $post->is_deleted = false;
      $post->updated_at = new \DateTime;
      $post->save();

      // Given a page index?
      $page = request('page');
      if (isset($page)) {
        // Show a page in the list
        return redirect("/admin?page=$page");
      }
              
      // Show the list
      return $this->index();
    }
    
    /**
     * Show the edit post 
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        // Get all images and thumbnails
        $images = $this->images;
        $thumbs = $this->thumbs;
      
        // Show its post 
        return view('admin.edit', compact('post', 'images', 'thumbs'));
    }

    /**
     * Update a specific validated post
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
      try {
        // Call from function request instead of a variable request
        $this->validate(request(), [
          'page' => 'required',
          'title' => 'required|min:2|max:255',
          'body' => 'required|min:20',
          'featured_image' => 'required',
          'thumbnail_image' => 'required'
        ]);
      }
      catch (ValidationException $e) {
        // Show the errors in the edit page
        return view('admin.edit');
      }
      
      $post = Post::find($id);
      $post->title = request('title');
      $post->body = request('body');
      $post->featured_image = request('featured_image');
      $post->thumbnail_image = request('thumbnail_image');
      $post->updated_at = new \DateTime;
      $post->save();
        
      // Show its post 
      return redirect("/admin?page=".request('page'));
    }


}
