<?php

namespace App\Http\Controllers;

use Validator;
use Auth;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\ValidationException;

/*
This CommentsController will handle all comment requests
*/
class CommentsController extends Controller
{
    /**
     * Store a newly created comment
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($postid)
    {
      try {
        // Call from function request instead of a variable request
        // Check the validation for all form fields
        $this->validate(request(), [
          'body' => 'required'
        ]);

      }
      catch (ValidationException $e) {
        // Display a message of required errors
        return redirect("/posts/$postid");
      }
      
      // Validation is OK?
      Comment::create([
        // Current logged-in user id
        'user_id' => Auth::id(),
        // Get the post id
        'post_id' => $postid,
        // Be sure to sanitize data before inserting into the database.
        'body' => filter_var(request('body'), FILTER_SANITIZE_STRING)
      ]);

      // Show the user post
      return redirect("/posts/$postid");

    }

}
