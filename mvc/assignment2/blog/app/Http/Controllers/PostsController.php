<?php

namespace App\Http\Controllers;

use Validator;
use App\Post;
use Illuminate\Http\Request;

/*
This PostsController will handle all post requests
*/
class PostsController extends Controller
{
    // images and thumbnails array
    private $images;
    private $thumbs;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // List all images from folder images
        $list = glob("./img/image*.jpg");
        $this->images = array();
        foreach ($list as $l) {
          $this->images[] = substr($l, 6);
        }
        // List all thumbnails from folder images
        $list = glob("./img/thumb*.jpg");
        $this->thumbs = array();
        foreach ($list as $l) {
          $this->thumbs[] = substr($l, 6);
        }
        
    }
    
    
    /**
     * Display a home page for all posts
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
      // Show only published and not deleted posts
      $posts = Post::latest()->where('status', 'published')->where('is_deleted', 0)->limit(4)->get();
      return view('home', compact('posts'));
    }
    
    /**
     * Display a main page for all posts
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Show only published and not deleted posts
        $posts = Post::latest()->where('status', 'published')->where('is_deleted', 0)->paginate(5);
        
        // Set a returned url to come back as a flash message
        session()->put('backurl', url()->full());
        // Display them
        return view('posts.index', compact('posts'));
    }

    /**
     * Generate a form for create a post
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Get all images and thumbnails
        $images = $this->images;
        $thumbs = $this->thumbs;
      
        // Show user post form
        return view('posts.create', compact('images', 'thumbs'));
    }

    /**
     * Create a post information and validate all binding requests
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
      try {
        // Call from function request instead of a variable request
        $this->validate(request(), [
          'title' => 'required|min:2|max:255',
          'body' => 'required|min:20',
          'featured_image' => 'required',
          'thumbnail_image' => 'required'
        ]);
      }
      catch (ValidationException $e) {
        // Show the errors in the create page
        return view('posts.create');
      }
      

      // Create the post with default image and thumbnail
      Post::create([
        'title' => $request['title'],
        'body' => $request['body'],
        'featured_image' => $request['featured_image'],
        'thumbnail_image' => $request['thumbnail_image']
      ]);
      
      // Back to the list based on the backurl
      $backurl = $request->session()->get('backurl');
      
      return redirect(isset($backurl) ? $backurl : '/posts');
    }

    /**
     * Show a specific post
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        // Get comments for a post
        $comments = Post::find($post->id)->comment;
        // Show its post and comments
        return view('posts.show', compact('post', 'comments'));
    }

    /**
     * Get all archive in a specific time range
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function archive($year, $month)
    {    
      $posts = Post::filter(['year' => $year, 'month' => $month]);
      return view('posts.archive', compact('posts', 'year', 'month'));
      
    }

    /**
     * Show the edit post form
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        // Get all images and thumbnails
        $images = $this->images;
        $thumbs = $this->thumbs;
      
        // Show its post 
        return view('posts.edit', compact('post', 'images', 'thumbs'));
    }

    /**
     * Update a specific validated post
     *
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
      try {
        // Call from function request instead of a variable request
        $this->validate(request(), [
          'title' => 'required|min:2|max:255',
          'body' => 'required|min:20'
        ]);
      }
      catch (ValidationException $e) {
        // Show the errors in the create page
        return view('posts.edit');
      }
      
      $post = Post::find($id);
      $post->title = request('title');
      $post->body = request('body');
      $post->save();
        
      // Show its post 
      return redirect("/posts/$id");
    }


}
