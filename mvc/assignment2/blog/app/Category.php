<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Category model
class Category extends Model
{
    // Return the many side. In this case all posts belonged to a category will be returned
    public function posts() {
      return $this->belongsToMany(Post::class);
    }
}
