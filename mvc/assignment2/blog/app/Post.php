<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

// Post model
class Post extends Model
{
    // Here is the whitelist
    protected $fillable = ['body', 'title', 'featured_image', 'thumbnail_image'];
    
    // Return a collection of comments
    public function comment() {
      return $this->hasMany(Comment::class);
    }

    // Return a collection of categories
    public function categories() {
      return $this->belongsToMany(Category::class);
    }

/*
    // This is a testing function
    public function go() {
      return $this->join('Comments', 'posts.id', '=', 'comments.post_id')
        ->select('comments.*', 'posts.*')->get();
    }


    // This is a testing function
    public function scopeThai($query) {
      return $query->join('Comments', 'posts.id', '=', 'comments.post_id')
        ->select('comments.*')->get();
    }
*/
    // Return a collection of Post that is created in a specific range of time
    public function scopeFilter($query, $params) {
      // Conver month string to zero leading string
      $month = substr('0'.Carbon::parse($params['month'])->month, -2);
      return Post::latest()
        ->whereRaw( "strftime('%Y%m', created_at) = '{$params['year']}$month'" )
        ->get();
    }
    
    // Return all archives of all posts 
    public static function archives() {
      return Post::latest()
      // Try to convert number to month names!!!
        ->selectRaw( "strftime('%Y', created_at) year, ".
          "rtrim (substr ('January  February March    April    May      June     July     August   SeptemberOctober  November December', strftime ('%m', created_at) * 9 - 8, 9)) month, ".
          "count(*) num")
        ->groupBy('year')
        ->groupBy('month')
        ->get()
        ->toArray();
    }
}
