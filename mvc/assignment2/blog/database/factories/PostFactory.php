<?php

use Faker\Generator as Faker;

$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
        'featured_image' => 'image'.rand(1,5).'.jpg',
        'thumbnail_image' => 'thumb'.rand(1,5).'.jpg',       
        'created_at' => $faker->dateTimeThisYear,
        'updated_at' => new DateTime
    ];
    
    
/*
        'created_at' => $faker->dateTimeThisYear,
        'updated_at' => new DateTime
        
//         'username' => $faker->userName,
//        'email' => $faker->email,
//        'name' => $faker->name    
        
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
        
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),        
            


        $faker->randomDigit;
        $faker->numberBetween(1,100);
        $faker->word;
        $faker->paragraph;
        $faker->lastName;
        $faker->city;
        $faker->year;
        $faker->domainName;
        $faker->creditCardNumber;

        */
});
