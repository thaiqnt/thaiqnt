<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(seed_posts_table::class);
        $this->command->info('Post table seeded!');
        $this->call(seed_users_table::class);
        $this->command->info('User table seeded!');
        $this->call(seed_categories_table::class);
        $this->command->info('Category table seeded!');
        $this->call(seed_categorypivots_table::class);
        $this->command->info('CategoryPivot table seeded!');
    }
}
