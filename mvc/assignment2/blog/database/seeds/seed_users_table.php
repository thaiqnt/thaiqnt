<?php

use Illuminate\Database\Seeder;

class seed_users_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // There are two users as requirement
        DB::table('users')->insert([
          'name' => 'john',
          'email' => 'john@example.com',
          'password' => bcrypt('mypass'),
          'is_admin' => 1
        ]);
        DB::table('users')->insert([
          'name' => 'thai',
          'email' => 'thaiqnt@gmail.com',
          'password' => bcrypt('123456'),
          'is_admin' => 0
        ]);
        //factory(\App\User::class, 3)->create();
    }
}
