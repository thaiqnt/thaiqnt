<?php

use Illuminate\Database\Seeder;

class seed_posts_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
      
        // Create 5 posts as requirement
        DB::table('posts')->insert([
          'title' => $faker->sentence,
          'body' => $faker->paragraph,
          'featured_image' => 'image1.jpg',
          'thumbnail_image' => 'thumb1.jpg',
          'status' => 'published',
          'is_deleted' => false,
          'created_at' => $faker->dateTimeThisYear,
          'updated_at' => new DateTime
        ]);
      
        DB::table('posts')->insert([
          'title' => $faker->sentence,
          'body' => $faker->paragraph,
          'featured_image' => 'image2.jpg',
          'thumbnail_image' => 'thumb2.jpg',
          'status' => 'published',
          'is_deleted' => false,
          'created_at' => $faker->dateTimeThisYear,
          'updated_at' => new DateTime
        ]);
      
        DB::table('posts')->insert([
          'title' => $faker->sentence,
          'body' => $faker->paragraph,
          'featured_image' => 'image3.jpg',
          'thumbnail_image' => 'thumb3.jpg',
          'status' => 'published',
          'is_deleted' => false,
          'created_at' => $faker->dateTimeThisYear,
          'updated_at' => new DateTime
        ]);
      
        DB::table('posts')->insert([
          'title' => $faker->sentence,
          'body' => $faker->paragraph,
          'featured_image' => 'image4.jpg',
          'thumbnail_image' => 'thumb4.jpg',
          'status' => 'draft',
          'is_deleted' => true,
          'created_at' => $faker->dateTimeThisYear,
          'updated_at' => new DateTime
        ]);
      
        DB::table('posts')->insert([
          'title' => $faker->sentence,
          'body' => $faker->paragraph,
          'featured_image' => 'image5.jpg',
          'thumbnail_image' => 'thumb5.jpg',
          'status' => 'published',
          'is_deleted' => false,
          'created_at' => $faker->dateTimeThisYear,
          'updated_at' => new DateTime
        ]);
      
        // Remove as the requirement requested
        // factory(\App\Post::class, 50)->create();
    }
}
