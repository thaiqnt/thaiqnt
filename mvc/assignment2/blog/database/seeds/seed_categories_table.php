<?php

use Illuminate\Database\Seeder;

class seed_categories_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $names = array(
        'PHP',
        'Java',
        'C#',
        'Scala',
        'Python'
      );

      foreach ($names as $name)
          DB::table('categories')->insert([
            'name' => $name,
            'created_at' => new DateTime,
            'updated_at' => new DateTime            
          ]);

    }
}
