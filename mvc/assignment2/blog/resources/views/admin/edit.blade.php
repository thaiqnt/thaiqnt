@extends('layouts.master_home')

@section('content')

 <!-- Blog Entries Column -->
        <div class="col-md-12">

          <h1 class="my-4">Review the post:
            
          </h1>

          
          <!-- Blog Post -->
          <div class="card mb-4">
            <div class="card-body">
            @if(Auth::check())
              <form action="/admin/{{$post->id}}/edit" method="post">
              <input type="hidden" name="page" value="{{Request::get('page')}}"/>
              @csrf

              @include('layouts.partials.errors')

              <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" id="title" name="title" value="{{old('title', $post->title)}}"/>
              </div>


              <div class="form-group">
              <label for="body">Post</label>
              <textarea id="body" name="body" class="form-control" rows="3">{{old('body', $post->body)}}</textarea>
              </div>

              <div class="form-group row">
                
                <div class="col-md-6">
                  <div class="dropdown">
                    <button $inputattr class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" >&nbsp;&nbsp;&nbsp;Post Image&nbsp;&nbsp;&nbsp;
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      @foreach($images as $image)
                      <li>
                        <a href="javascript: changeImage('{{$image}}');">
                          <img src="/img/{{$image}}" alt="{{$image}}" width="60" /></a>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                  <br/>
                  <img id= "shownimage" src='/img/{{old('featured_image', $post->featured_image)}}' alt='{{old('featured_image', $post->featured_image)}}' width="120" />
                  <input type='hidden' id= "postedimage" name="featured_image" value='{{old('featured_image', $post->featured_image)}}'/>
                </div>

                <div class="col-md-6">
                  <div class="dropdown">
                    <button $inputattr class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" >&nbsp;&nbsp;&nbsp;Thumb Image&nbsp;&nbsp;&nbsp;
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      @foreach($thumbs as $thumb)
                      <li>
                        <a href="javascript: changeThumb('{{$thumb}}');">
                          <img src="/img/{{$thumb}}" alt="{{$thumb}}" width="60" /></a>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                  <br/>
                  <img id= "shownthumb" src='/img/{{old('thumbnail_image', $post->thumbnail_image)}}' alt='{{old('thumbnail_image', $post->thumbnail_image)}}' width="120" />
                  <input type='hidden' id= "postedthumb" name="thumbnail_image" value='{{old('thumbnail_image', $post->thumbnail_image)}}'/>
                </div>
              </div>

                
              <div class="form-group">
              <button type="submit" class="btn btn-primary" name="update">Update</button>
              @if($post->status=='draft')
              <a href="/admin/{{$post->id}}/publish?page={{Request::get('page')}}" class="btn btn-primary">Publish</a>
              @else
              <a href="/admin/{{$post->id}}/unpublish?page={{Request::get('page')}}" class="btn btn-primary">Unpublish</a>
              @endif
              @if($post->is_deleted)
              <a href="/admin/{{$post->id}}/undelete?page={{Request::get('page')}}" class="btn btn-primary">Undelete</a>
              @else
              <a href="/admin/{{$post->id}}/delete?page={{Request::get('page')}}" class="btn btn-primary">Delete</a>
              @endif
              
              </div>

              </form>
            @else
            <div class="alert alert-danger">
              <ul>
                  <li>You are visiting this site as a guest, please <a href="{{@url('/login')}}">log in</a> to give us your valuable post.</li>
              </ul>
            </div>            

            @endif            
            </div>
            
          </div>
         

        </div>
        <script>

        function changeImage(filename) {
          document.getElementById('postedimage').value = filename;
          document.getElementById('shownimage').setAttribute('src', '/img/'+filename);
        }
          
        function changeThumb(filename) {
          document.getElementById('postedthumb').value = filename;
          document.getElementById('shownthumb').setAttribute('src', '/img/'+filename);
        }
        </script>
@endsection
