@extends('layouts.master_home')

@section('content')
      <div class="col-lg-12">
        <h1 class="my-4"><a href="{{@url('/posts/create')}}" class="btn btn-primary">Create your POST!</a>
        <!-- 
        {{$page=Request::get('page')?Request::get('page'):1}}
        -->
        </h1>
      </div>
      <!-- Page Features -->
      <div class="row text-center">
        @foreach($posts as $post)
        <div class="col-lg-3 col-md-3 mb-4">
          <a href="/admin/{{$post->id}}/edit?page={{$page}}" style="color: black;"><strong>{{substr($post->created_at,0,62)}}</strong></a>
        </div>
        <div class="col-lg-6 col-md-6 mb-4">
          <a href="/admin/{{$post->id}}/edit?page={{$page}}" style="color: green;"><strong>{{substr($post->title,0,112)}}</strong></a>
        </div>
        <div class="col-lg-3 col-md-3 mb-4">
          @if($post->status=='draft')
          <a href="/admin/{{$post->id}}/publish?page={{$page}}" class="btn btn-primary">Publish</a>
          @else
          <a href="/admin/{{$post->id}}/unpublish?page={{$page}}" class="btn btn-primary">Unpublish</a>
          @endif
          @if($post->is_deleted)
          <a href="/admin/{{$post->id}}/undelete?page={{$page}}" class="btn btn-primary">Undelete</a>
          @else
          <a href="/admin/{{$post->id}}/delete?page={{$page}}" class="btn btn-primary">Delete</a>
          @endif
        </div>
      @endforeach
      <!-- Pagination -->
      <ul class="pagination justify-content-center mb-4">
        {{$posts->links()}}
      </ul>

      </div>
      <!-- /.row -->
@endsection
