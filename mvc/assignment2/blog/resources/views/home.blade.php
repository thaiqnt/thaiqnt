@extends('layouts.master_home')

@section('content')
@if(count($posts)>0)
      <!-- Jumbotron Header -->
      <header class="jumbotron my-4">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <!-- {{$active='class=\"active\"'}} {{$i=0}} -->
            @foreach($posts as $post)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{$i++}}" {{$active}}></li>
            {{$active=''}}
            @endforeach
          </ol>
          <div class="carousel-inner">
            <!-- {{$active='active'}} -->
            @foreach($posts as $post)
            <div class="carousel-item {{$active}}">
              <a href="/posts/{{$post->id}}">
              <img class="d-block w-100" src="/img/{{$post->featured_image}}" alt="/img/{{$post->featured_image}}">
              </a>
              <div class="carousel-caption d-none d-md-block overrainbow">
                <h5>{{substr($post->title,0,39)}}</h5>
                <p>{{substr($post->body,0,113)}}</p>
              </div>              
            </div>
            {{$active=''}}
            @endforeach
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </header>

      <!-- Page Features -->
      <div class="row text-center">
      @foreach($posts as $post)
        <div class="col-lg-3 col-md-6 mb-4">
          <div class="card overrainbow">
            <a href="/posts/{{$post->id}}">
            <img class="card-img-top" src="/img/{{$post->thumbnail_image}}" alt="">
            </a>
            <div class="card-body">
              <h4 class="card-title"><a href="/posts/{{$post->id}}">{{substr($post->title,0,32)}}</a></h4>
              <p class="card-text"><a href="/posts/{{$post->id}}">{{substr($post->body,0,94)}}</a></p>
            </div>
            <div class="card-footer">
              <a href="/posts/{{$post->id}}" class="btn btn-primary">Find Out More!</a>
            </div>
          </div>
        </div>
      @endforeach

      </div>
      <!-- /.row -->
@else
      <div class="col-lg-12 col-md-12 mb-4">
        <div class="card">
          <div class="card-body">
            <p class="card-text text-center">There is no post to show, please <a href="/admin" class="btn btn-primary">come here as an administrator</a> to review all posts.</p>
          </div>
        </div>
      </div>
@endif
@endsection
