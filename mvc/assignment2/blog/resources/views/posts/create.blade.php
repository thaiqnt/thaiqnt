@extends('layouts.master')


@section('content')
 <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Bring your idea to our place!
            
          </h1>

          
          <!-- Blog Post -->
          <div class="card mb-4">
            <div class="card-body">
            @if(Auth::check())
              <form action="{{@url('/posts')}}" method="post">
              @csrf

              @include('layouts.partials.errors')



              <div class="form-group">
              <label for="title">Title</label>
              <input type="text" class="form-control" id="title" name="title"value="{{old('title', '')}}"/>
              </div>


              <div class="form-group">
              <label for="body">Post</label>
              <textarea id="body" name="body" class="form-control" rows="3">{{old('body', '')}}</textarea>
              </div>

              <div class="form-group row">
                
                <div class="col-md-6">
                  <div class="dropdown">
                    <button $inputattr class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" >&nbsp;&nbsp;&nbsp;Post Image&nbsp;&nbsp;&nbsp;
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      @foreach($images as $image)
                      <li>
                        <a href="javascript: changeImage('{{$image}}');">
                          <img src="/img/{{$image}}" alt="{{$image}}" width="60" /></a>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                  <br/>
                  <img id= "shownimage" src='/img/{{old('featured_image', 'image1.jpg')}}' alt='{{old('featured_image', 'image1.jpg')}}' width="120" />
                  <input type='hidden' id= "postedimage" name="featured_image" value='{{old('featured_image', 'image1.jpg')}}'/>
                </div>

                <div class="col-md-6">
                  <div class="dropdown">
                    <button $inputattr class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" >&nbsp;&nbsp;&nbsp;Thumb Image&nbsp;&nbsp;&nbsp;
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      @foreach($thumbs as $thumb)
                      <li>
                        <a href="javascript: changeThumb('{{$thumb}}');">
                          <img src="/img/{{$thumb}}" alt="{{$thumb}}" width="60" /></a>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                  <br/>
                  <img id= "shownthumb" src='/img/{{old('thumbnail_image', 'thumb1.jpg')}}' alt='{{old('thumbnail_image', 'thumb1.jpg')}}' width="120" />
                  <input type='hidden' id= "postedthumb" name="thumbnail_image" value='{{old('thumbnail_image', 'thumb1.jpg')}}'/>
                </div>
              </div>

                
              <div class="form-group">
              <button type="submit" class="btn btn-primary" name="publish">Submit for publishing</button>
              </div>

              </form>
            @else
            <div class="alert alert-danger">
              <ul>
                  <li>You are visiting this site as a guest, please <a href="{{@url('/login')}}">log in</a> to give us your valuable post.</li>
              </ul>
            </div>            

            @endif            
            </div>
            

            <div class="card-footer text-muted">
              Posted on Oct 18, 2018 by
              <a href="#">WDD Blogger</a>
            </div>
          </div>
         
          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>

        </div>

        @include('layouts.partials.sidebar')

        <script>

        function changeImage(filename) {
          document.getElementById('postedimage').value = filename;
          document.getElementById('shownimage').setAttribute('src', '/img/'+filename);
        }
          
        function changeThumb(filename) {
          document.getElementById('postedthumb').value = filename;
          document.getElementById('shownthumb').setAttribute('src', '/img/'+filename);
        }
        </script>
@endsection