@extends('layouts.master')


@section('content')

 <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4"><a href="{{@url('/posts')}}">Back to all the posts.</a>
            
          </h1>

          
          <!-- Blog Post -->
          <div class="card mb-4">
            <img class="card-img-top" src="/img/{{$post->featured_image}}" alt="Card image cap" width="750">
            <div class="card-body">
              <h2 class="card-title"><a href="/posts/{{$post->id}}/edit">{{$post->title}}</a></h2>
              <p class="card-text">{{$post->body}}</p>
              <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Read More &rarr;</a>
            </div>
            
            @if(Auth::check())
            <!-- Comments Form -->
            <div class="card my-4">
              <h5 class="card-header">Leave a Comment:</h5>
              <div class="card-body">
                @include('layouts.partials.errors')
                <form action="/posts/{{$post->id}}/comments" method="post">
                @csrf
                  <div class="form-group">
                    <textarea name="body" class="form-control" rows="3"></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
            @else
            <div class="alert alert-danger">
              <ul>
                  <li>You are visiting this site as a guest, please <a href="{{@url('/login')}}">log in</a> to post a new comment.</li>
              </ul>
            </div>            
            @endif            

            <!-- Single Comment -->
            @foreach($comments as $comment)
            <div class="media mb-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
              <div class="media-body">
                <h5 class="mt-0">{{App\User::find($comment->user_id)->name}}</h5>
                {{$comment->body}}
              </div>
            </div>
            @endforeach                
            
            <div class="card-footer text-muted">
              Posted on Oct 18, 2018 by
              <a href="#">WDD Blogger</a>
            </div>
          </div>
         
          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>

        </div>

        @include('layouts.partials.sidebar')

@endsection