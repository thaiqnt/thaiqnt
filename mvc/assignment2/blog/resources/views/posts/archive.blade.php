@extends('layouts.master')


@section('content')

 <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4"><a href="{{@url('#')}}">{{$year}} / {{$month}}</a>
            
          </h1>

          
          <!-- Blog Post -->
          <div class="card mb-4">
            <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
            <div class="card-body">
            @foreach($posts as $post)
              <h2 class="card-title"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h2>
              <p class="card-text">{{$post->body}}</p>
              <a href="#" class="btn btn-primary">Read More &rarr;</a>
            @endforeach
            </div>
            <div class="card-footer text-muted">
              Posted on Oct 18, 2018 by
              <a href="#">WDD Blogger - See all posts here</a>
            </div>
          </div>
         
          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="#">&larr; Older</a>
            </li>
            <li class="page-item disabled">
              <a class="page-link" href="#">Newer &rarr;</a>
            </li>
          </ul>

        </div>

        @include('layouts.partials.sidebar')

@endsection