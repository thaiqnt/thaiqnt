<!DOCTYPE html>
<html lang="en">

  <head>

    <!-- THis is my master blade -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Home - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/app.css" type="text/css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="/css/blog.css" type="text/css" rel="stylesheet" />

  </head>

  <body>

    @include('layouts.partials.nav')

    <!-- Page Content -->
    <div class="container">

      <div class="row">

       @yield('content')

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; {{ date('Y') }} by WDD Blogger</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/js/app.js"></script>

  </body>

</html>
