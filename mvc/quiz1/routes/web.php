<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/{link}', function ($link) {
    // all data of the site
    $data = array(
      'home' => 'This is the quizz1 of the MVC class introduced by Steve George',
      'about' => 'This site is developed by Thai Tran Ngoc Quoc (c) 2018',
      'support' => 'Just call Steve for your request',
      'services' => 'We provide a support service at premium price',
      'contact' => 'Call 911'
    );
    
    // the keys are the links
    $url = array_keys($data);
    
    // try to locate a title from data
    if (in_array($link, $url)) {
      $title = $data[$link];
    } else {
      // if not found, set title as default title as following
      $title = 'Sorry, there is an unavailable request, could you please come back and try another one?';
    }
    // Use a single page as the requirement
    return view('index', compact('url', 'title'));
});

